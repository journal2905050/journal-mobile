import 'package:creetech/Admin/Dashboard/dashboardscreen.dart';
import 'package:creetech/Login/loginscreen.dart';
import 'package:creetech/Admin/Sidebar/Pages/Kelas/DaftarMurid/FilterSiswa/filter_siswa.dart';
import 'package:creetech/Admin/Sidebar/Pages/Kelas/DaftarMurid/MutasiSiswa/mutasi_siswa.dart';
import 'package:creetech/Admin/Sidebar/Pages/Kelas/DaftarMurid/daftar_murid.dart';
import 'package:creetech/GuruBK/Dashboard/dashboard.dart';
import 'package:creetech/GuruBK/Sidebar/Page/Presensi/presensi_bk.dart';
import 'package:creetech/Pengajar/Dashboard/dashboard.dart';
import 'package:creetech/Siswa/Dashboard/dashboard.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DaftarMurid(),
    );
  }
}

