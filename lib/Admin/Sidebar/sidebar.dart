import 'package:creetech/Admin/Dashboard/dashboardscreen.dart';
import 'package:creetech/Admin/Sidebar/Pages/Kelas/DaftarKelas/daftar_kelas.dart';
import 'package:creetech/Admin/Sidebar/Pages/Kelas/DaftarMurid/daftar_murid.dart';
import 'package:creetech/Admin/Sidebar/Pages/Pengguna/Karyawan/karyawan.dart';
import 'package:creetech/Admin/Sidebar/Pages/Pengguna/Siswa/siswa.dart';
import 'package:creetech/Admin/Sidebar/Pages/PeriodeAkademik/periode_akademik.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Sidebar extends StatefulWidget {
  const Sidebar({super.key});

  @override
  State<Sidebar> createState() => _SidebarState();
}

class _SidebarState extends State<Sidebar> {
  String _selectedRole = '';

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Color(0xFF232A3F),
      child: Column(
        children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: Color(0xFF232A3F),
                  ),
                  child: Row(
                    children: [
                      Image.asset(
                        'assets/images/tutwuri.png',
                        width: 80,
                        height: 80,
                      ),
                      SizedBox(width: 16),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'SMA Negeri 1 Ambarawa',
                              style: TextStyle(
                                  fontFamily: "OpenSans",
                                  color: Colors.white,
                                  fontSize: 24,
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(height: 10),
                            Text(
                              'Admin',
                              style: TextStyle(
                                fontFamily: "OpenSans",
                                color: Color(0xFF8B8B8B),
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                DashedDivider(
                  height: 2,
                  color: Color(0xFF8B8B8B),
                  thickness: 1,
                  dashLength: 8,
                  dashGap: 5,
                  indent: 10,
                  endIndent: 10,
                ),
                ListTile(
                  title: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/images/home.svg',
                        width: 20,
                        height: 20,
                      ),
                      SizedBox(width: 10),
                      Text(
                        'Dashboard',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DashboardScreen()),
                    );
                  },
                ),
                ListTile(
                  title: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/images/calendar.svg',
                        width: 20,
                        height: 20,
                      ), // Icon home di sini
                      SizedBox(width: 10),
                      Text(
                        'Periode Akademik',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PeriodeAkademik()),
                    );
                  },
                ),
                ExpansionTile(
                  title: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/images/users.svg',
                        width: 20,
                        height: 20,
                      ),
                      SizedBox(width: 10),
                      Text(
                        'Pengguna',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  trailing: Container(
                    width: 0,
                  ),
                  children: [
                    ListTile(
                      title: Text(
                        'Karyawan',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                      onTap: () {
                        // Handle Karyawan menu tap
                        setState(() {
                          _selectedRole = 'Karyawan';
                        });
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Karyawan()),
                        );
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Siswa',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                      onTap: () {
                        // Handle Siswa menu tap
                        setState(() {
                          _selectedRole = 'Siswa';
                        });
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Siswa()),
                        );
                      },
                    ),
                  ],
                ),
                ExpansionTile(
                  title: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/images/logo3.svg',
                        width: 20,
                        height: 20,
                      ),
                      SizedBox(width: 10),
                      Text(
                        'Kelas',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  trailing: Container(
                    width: 0,
                  ),
                  children: [
                    ListTile(
                      title: Text(
                        'Daftar Kelas',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                      onTap: () {
                        // Handle Karyawan menu tap
                        setState(() {
                          _selectedRole = 'Daftar Kelas';
                        });
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DaftarKelas()),
                        );
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Daftar Murid',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          color: Colors.white,
                        ),
                      ),
                      onTap: () {
                        // Handle Siswa menu tap
                        setState(() {
                          _selectedRole = 'Daftar Murid';
                        });
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DaftarMurid()),
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Row(
              children: [
                SvgPicture.asset(
                  'assets/images/logout.svg',
                  width: 20,
                  height: 20,
                ),
                SizedBox(width: 8),
                Text(
                  'Logout',
                  style: TextStyle(
                    fontFamily: "OpenSans",
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            onTap: () {
              // Handle logout menu tap
              Navigator.pop(context);
              // Add your logout logic here
            },
          ),
        ],
      ),
    );
  }
}

class DashedDivider extends StatelessWidget {
  final double height;
  final Color color;
  final double thickness;
  final double dashLength;
  final double dashGap;
  final double indent;
  final double endIndent;

  const DashedDivider({
    Key? key,
    required this.height,
    required this.color,
    required this.thickness,
    required this.dashLength,
    required this.dashGap,
    required this.indent,
    required this.endIndent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: CustomPaint(
        painter: DashedLinePainter(
          color: color,
          thickness: thickness,
          dashLength: dashLength,
          dashGap: dashGap,
        ),
        child: Container(),
      ),
      margin: EdgeInsets.only(left: indent, right: endIndent),
    );
  }
}

class DashedLinePainter extends CustomPainter {
  final Color color;
  final double thickness;
  final double dashLength;
  final double dashGap;

  DashedLinePainter({
    required this.color,
    required this.thickness,
    required this.dashLength,
    required this.dashGap,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = color
      ..strokeWidth = thickness;

    const double space = -2; // Jarak awal sebelum garis pertama

    double startX = space;
    double endX = dashLength;

    while (endX < size.width) {
      canvas.drawLine(Offset(startX, size.height / 2),
          Offset(endX, size.height / 2), paint);
      startX = endX + dashGap;
      endX = startX + dashLength;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
