import 'package:creetech/Admin/Sidebar/Pages/Kelas/DaftarKelas/daftar_kelas.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FilterTingkatan extends StatefulWidget {
  const FilterTingkatan({super.key});

  @override
  State<FilterTingkatan> createState() => _FilterTingkatanState();
}

class _FilterTingkatanState extends State<FilterTingkatan> {
  late TextEditingController _textEditingController;
  List<String> dropdownOptions = ['2024/2025', '2023/2024', '2022/2021'];
  String? selectedOption;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
  }

  void _showSuccessDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        Future.delayed(Duration(seconds: 2), () {
          Navigator.of(context).pop(); // Tutup dialog setelah 2 detik
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  DaftarKelas(), // Ganti dengan nama page yang sesuai
            ),
          );
        });

        return AlertDialog(
          backgroundColor: Color(0xFF232A3F),
          insetPadding: EdgeInsets.all(10),
          contentPadding: EdgeInsets.zero,
          content: Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 5),
            width: 350,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Selamat!',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 24,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(height: 30),
                Divider(
                  color: Color(0xFF4E4E4E),
                  thickness: 1,
                ),
                SizedBox(height: 25),
                SvgPicture.asset(
                  'assets/images/succes.svg',
                ),
                SizedBox(height: 30),
                Text(
                  'Filter berhasil\nditerapkan',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      insetPadding: EdgeInsets.all(20),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 20,
        ),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Color(0xFF232A3F),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Filter Tingkatan',
              style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  fontFamily: 'OpenSans'),
            ),
            SizedBox(
              height: 15,
            ),
            Divider(
              color: Color(0xFF4E4E4E),
              thickness: 1,
            ),
            SizedBox(height: 20),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: DropdownButtonFormField<String>(
                
                value: selectedOption,
                onChanged: (String? newValue) {
                  setState(() {
                    selectedOption = newValue;
                  });
                },
                items: dropdownOptions.map((String option) {
                  return DropdownMenuItem<String>(
                    value: option,
                    child: Text(
                      option,
                      style: TextStyle(color: Colors.white),
                    ),
                  );
                }).toList(),
                decoration: InputDecoration(
                  hintText: 'Tahun Ajaran',
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFF8B8B8B),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hintStyle: TextStyle(
                    color: const Color.fromRGBO(255, 255, 255, 1),
                  ),
                  suffixIcon: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.white,
                  ),
                ),
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'OpenSans',
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                ),
                dropdownColor: Color(0xFF232A3F),
                // Set isDense to true and customize the padding
                isDense: true,
                iconSize: 30, // Adjust the icon size as needed
                itemHeight: 48, // Adjust the item height as needed
              ),
            ),
            SizedBox(height: 20),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: DropdownButtonFormField<String>(
                value: selectedOption,
                onChanged: (String? newValue) {
                  setState(() {
                    selectedOption = newValue;
                  });
                },
                items: dropdownOptions.map((String option) {
                  return DropdownMenuItem<String>(
                    value: option,
                    child: Text(
                      option,
                      style: TextStyle(color: Colors.white),
                    ),
                  );
                }).toList(),
                decoration: InputDecoration(
                  hintText: 'Tingkat',
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFF8B8B8B),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hintStyle: TextStyle(
                    color: const Color.fromRGBO(255, 255, 255, 1),
                  ),
                  suffixIcon: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.white,
                  ),
                ),
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'OpenSans',
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                ),
                dropdownColor: Color(0xFF232A3F),
                // Set isDense to true and customize the padding
                isDense: true,
                iconSize: 30, // Adjust the icon size as needed
                itemHeight: 48, // Adjust the item height as needed
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Divider(
              color: Color(0xFF4E4E4E),
              thickness: 1,
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 130,
                    minHeight: 60,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/images/cancel.svg'),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.transparent),
                        ),
                        child: Text(
                          'Cancel',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.blue,
                  ),
                  constraints: BoxConstraints(
                    minWidth: 130,
                    minHeight: 60,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/images/yes.svg'),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          _showSuccessDialog();
                        },
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.transparent),
                        ),
                        child: Text(
                          'Add',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
