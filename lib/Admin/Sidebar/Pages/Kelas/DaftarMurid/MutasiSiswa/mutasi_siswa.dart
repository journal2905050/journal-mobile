import 'package:creetech/Admin/Sidebar/Pages/Kelas/DaftarMurid/daftar_murid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class MutasiSiswa extends StatefulWidget {
  const MutasiSiswa({super.key});

  @override
  State<MutasiSiswa> createState() => _MutasiSiswaState();
}

class _MutasiSiswaState extends State<MutasiSiswa> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF191925),
        appBar: AppBar(
          backgroundColor: Color(0xFF232A3F),
          title: Text(
            'Mutasi Siswa',
            style: TextStyle(
                fontFamily: "OpenSans",
                fontSize: 20,
                fontWeight: FontWeight.w400),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: CircleAvatar(
                backgroundImage: AssetImage('assets/images/33.jpg'),
              ),
              onPressed: () {},
            ),
          ],
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: Image.asset(
              'assets/images/back1.png',
              width: 20,
              height: 20,
            ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          }),
        ),
        drawer: DaftarMurid(),
        body: Column(
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 20),
                height: 55,
                width: 370,
                decoration: BoxDecoration(
                  color: Color(0xFF232A3F),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.keyboard_arrow_down, color: Colors.white),
                    Text(
                      'Pilih Siswa',
                      style: TextStyle(
                          fontFamily: "OpenSans",
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 180),
            Center(
              child: Container(
                child: Column(
                  children: [
                    SvgPicture.asset(
                      'assets/images/empty.svg',
                    ),
                    SizedBox(height: 20),
                    Text('Belum ada Siswa\nyang dipilih',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: "OpenSans",
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Colors.white)),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
