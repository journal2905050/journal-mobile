import 'package:creetech/Admin/Sidebar/Pages/Pengguna/Siswa/TambahkanSiswa/tambah_siswa.dart';
import 'package:creetech/Admin/Sidebar/sidebar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Siswa extends StatefulWidget {
  const Siswa({super.key});

  @override
  State<Siswa> createState() => _SiswaState();
}

class _SiswaState extends State<Siswa> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        bottomSheet: Container(
          padding: EdgeInsets.all(20),
          color: Color(0xFF232A3F),
          width: double.infinity,
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => TambahSiswa()),
              );
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              height: 60,
              decoration: BoxDecoration(
                color: Color(0xFF015FFB),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: SvgPicture.asset(
                      'assets/images/add.svg',
                    ),
                  ),
                  Text(
                    'SISWA',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        backgroundColor: Color(0xFF191925),
        appBar: AppBar(
          backgroundColor: Color(0xFF232A3F),
          title: Text(
            'Siswa',
            style: TextStyle(
                fontFamily: "OpenSans",
                fontSize: 20,
                fontWeight: FontWeight.w400),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: CircleAvatar(
                backgroundImage: AssetImage('assets/images/33.jpg'),
              ),
              onPressed: () {},
            ),
          ],
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: SvgPicture.asset(
                'assets/images/burger.svg',
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          }),
        ),
        drawer: Sidebar(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Color(0xFF8B8B8B),
                    width: 1,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      'assets/images/search.svg',
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      child: TextField(
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: "OpenSans",
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                        decoration: InputDecoration(
                          hintText: "Cari",
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontFamily: "OpenSans",
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                          ),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                padding: EdgeInsets.only(left: 10, top: 20, right: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFF232A3F),
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Column(
                    children: [
                      Row(
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'NIS',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(width: 80),
                          Text(
                            'Nama',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(width: 90),
                          Text(
                            'Jenis Kelamin',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(width: 50),
                          Text(
                            'Tahun Masuk',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: 555,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        decoration: BoxDecoration(
                          color: Color(0xFF191925),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text(
                              '12345',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(width: 50),
                            Text(
                              'Rahayu',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(width: 80),
                            Text(
                              'Perempuan',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(
                              width: 90,
                            ),
                            Text(
                              '2021',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 555,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text(
                              '12345',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(width: 50),
                            Text(
                              'Rahayu',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(width: 80),
                            Text(
                              'Perempuan',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(
                              width: 90,
                            ),
                            Text(
                              '2021',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: 555,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        margin: EdgeInsets.only(bottom: 20),
                        decoration: BoxDecoration(
                          color: Color(0xFF191925),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text(
                              '12345',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(width: 50),
                            Text(
                              'Rahayu',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(width: 80),
                            Text(
                              'Perempuan',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(
                              width: 90,
                            ),
                            Text(
                              '2021',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 235,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
