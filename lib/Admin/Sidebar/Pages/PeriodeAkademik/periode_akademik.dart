import 'package:creetech/Admin/Sidebar/Pages/PeriodeAkademik/PeriodeDialog/periode_dialog.dart';
import 'package:creetech/Admin/Sidebar/sidebar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class PeriodeAkademik extends StatefulWidget {
  const PeriodeAkademik({super.key});

  @override
  State<PeriodeAkademik> createState() => _PeriodeAkademikState();
}

class _PeriodeAkademikState extends State<PeriodeAkademik> {
  int? selectedCheckboxIndex;
  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return PeriodeDialog(); // Use the PeriodeDialog here
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        bottomSheet: Container(
          padding: EdgeInsets.all(20),
          color: Color(0xFF232A3F),
          width: double.infinity,
          child: GestureDetector(
            onTap: () {
              _showDialog(context); // Pass the context to _showDialog
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              height: 60,
              decoration: BoxDecoration(
                color: Color(0xFF015FFB),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: SvgPicture.asset(
                      'assets/images/add.svg',
                    ),
                  ),
                  Text(
                    'PERIODE',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        backgroundColor: Color(0xFF191925),
        appBar: AppBar(
          backgroundColor: Color(0xFF232A3F),
          title: Text(
            'Periode Akademik',
            style: TextStyle(
                fontFamily: "OpenSans",
                fontSize: 20,
                fontWeight: FontWeight.w400),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: CircleAvatar(
                backgroundImage: AssetImage('assets/images/33.jpg'),
              ),
              onPressed: () {},
            ),
          ],
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: SvgPicture.asset(
                'assets/images/burger.svg',
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          }),
        ),
        drawer: Sidebar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Color(0xFF8B8B8B),
                    width: 1,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      'assets/images/search.svg',
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      child: TextField(
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "OpenSans",
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                        decoration: InputDecoration(
                          hintText: "Cari",
                          hintStyle: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                padding: EdgeInsets.only(left: 10, top: 20, right: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFF232A3F),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 6),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Tahun Ajaran',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(width: 50),
                          Text(
                            'Status',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(width: 80),
                          Text(
                            'Aksi',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "OpenSans",
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      margin: EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                        color: Color(0xFF191925),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(width: 20),
                          Text(
                            '2023/2024',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(width: 60),
                          Checkbox(
                            value: selectedCheckboxIndex == 0,
                            onChanged: (value) {
                              setState(() {
                                selectedCheckboxIndex = value! ? 0 : null;
                              });
                            },
                            checkColor: Colors.black,
                            activeColor: Color(0xFF83EE99),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(2),
                            ),
                            side: MaterialStateBorderSide.resolveWith(
                              (states) => BorderSide(
                                  width: 1, color: Color(0xFF83EE99)),
                            ),
                          ),
                          SizedBox(width: 75),
                          IconButton(
                            icon: SvgPicture.asset(
                              'assets/images/delete.svg',
                            ),
                            onPressed: () {},
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(width: 20),
                          Text(
                            '2023/2024',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(width: 60),
                          Checkbox(
                            value: selectedCheckboxIndex == 1,
                            onChanged: (value) {
                              setState(() {
                                selectedCheckboxIndex = value! ? 1 : null;
                              });
                            },
                            checkColor: Colors.black,
                            activeColor: Color(0xFF83EE99),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(2),
                            ),
                            side: MaterialStateBorderSide.resolveWith(
                              (states) => BorderSide(
                                  width: 1, color: Color(0xFF83EE99)),
                            ),
                          ),
                          SizedBox(width: 75),
                          IconButton(
                            icon: SvgPicture.asset(
                              'assets/images/delete.svg',
                            ),
                            onPressed: () {},
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      margin: EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                        color: Color(0xFF191925),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(width: 20),
                          Text(
                            '2023/2024',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "OpenSans",
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(width: 60),
                          Checkbox(
                            value: selectedCheckboxIndex == 2,
                            onChanged: (value) {
                              setState(() {
                                selectedCheckboxIndex = value! ? 2 : null;
                              });
                            },
                            checkColor: Colors.black,
                            activeColor: Color(0xFF83EE99),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(2),
                            ),
                            side: MaterialStateBorderSide.resolveWith(
                              (states) => BorderSide(
                                  width: 1, color: Color(0xFF83EE99)),
                            ),
                          ),
                          SizedBox(width: 75),
                          IconButton(
                            icon: SvgPicture.asset(
                              'assets/images/delete.svg',
                            ),
                            onPressed: () {},
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
