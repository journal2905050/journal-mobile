import 'package:creetech/Admin/Sidebar/sidebar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:d_chart/d_chart.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  List<OrdinalData> ordinalDataList = [
    OrdinalData(domain: 'Siswa', measure: 5, color: Color(0xFF83EE99)),
    OrdinalData(domain: 'Staff', measure: 3, color: Color(0xFF015FFB)),
  ];

  List<OrdinalData> ordinalDataListBar = [ 
    OrdinalData(domain: 'Siswa Laki-laki', measure: 80, color: Colors.green),
    OrdinalData(domain: 'Staff Laki-laki', measure: 60, color: Colors.orange),
    OrdinalData(domain: 'Siswi Perempuan', measure: 57, color: Colors.red),
    OrdinalData(domain: 'Siswa Perempuan', measure: 65, color: Colors.blueAccent)
  ];

  late final ordinalGroup = [
    OrdinalGroup(
      id: '1',
      data: ordinalDataListBar,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF191925),
      appBar: AppBar(
        backgroundColor: Color(0xFF232A3F),
        title: Text(
          'Dashboard',
          style: TextStyle(
              fontFamily: "OpenSans",
              fontSize: 20,
              fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: CircleAvatar(
              backgroundImage: AssetImage('assets/images/33.jpg'),
            ),
            onPressed: () {},
          ),
        ],
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: SvgPicture.asset(
              'assets/images/burger.svg',
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
          );
        }),
      ),
      drawer: Sidebar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 205,
            margin: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Color(0xFF232A3F),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Row(
              children: [
                //  <---- DIAGRAM LINGKARAN ---->
                Container(
                  width: 200, // Set a width for the chart
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      DChartPieO(
                        data: ordinalDataList,
                        configRenderPie: const ConfigRenderPie(
                          arcWidth: 10,
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "120",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 32,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            "Total Data",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  width: 50,
                ),
                // Text for Siswa and Staff
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xFF83EE99),
                          ),
                        ),
                        SizedBox(width: 8), // Adjust the spacing
                        Text(
                          'Siswa',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        // Colored dot for Staff
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xFF015FFB),
                          ),
                        ),
                        SizedBox(width: 8), // Adjust the spacing
                        Text(
                          'Staff',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),

          //  <---- DIAGRAM BATANG ---->
          Container(
            height: 280,
            margin: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Color(0xFF232A3F),
              borderRadius: BorderRadius.circular(12),
            ),
            child: DChartBarO(
              groupList: ordinalGroup,
              configRenderBar: ConfigRenderBar(
                maxBarWidthPx: 10,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
