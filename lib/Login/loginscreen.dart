// import 'package:creetech/Pages/Widgets/text_form_global.dart';
import 'package:creetech/Admin/Dashboard/dashboardscreen.dart';
import 'package:creetech/GuruBK/Dashboard/dashboard.dart';
import 'package:creetech/Pengajar/Dashboard/dashboard.dart';
import 'package:creetech/Siswa/Dashboard/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool obscureText = true;

  Future<void> _loginUser(
      BuildContext context, String email, String Password) async {
    final url = Uri.parse('https://journal-api.exiglosi.com/api/login');
    final response = await http.post(url,
        headers: <String, String>{'Content-Type': 'application/json'},
        body: jsonEncode(<String, String>{
          'username': emailController.text,
          'password': passwordController.text
        }));

    if (response.statusCode == 200) {
      // Login successful, handle response
      print('Response: ${response.statusCode}');
      print('Body: ${response.body}');
      print('Login successful');
      final responseData = jsonDecode(response.body);
      final role = responseData['data']['role'];

      if (role['is_bk'] == true) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => DashboardGuruBK()));
      } else if (role['is_admin'] == true) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => DashboardScreen()));
      } else if (role['is_employee'] == true) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => DashboardPengajar()));
      } else if (role['is_student'] == true) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => DashboardSiswa()));
      } else {
        throw Exception('User role not recognized');
      }
    } else {
      // Login failed, handle error

      print('Login failed');
      throw Exception('Failed to login');
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          backgroundColor: Color(0xFF191925),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 90),
                  child: Container(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Selamat Datang!",
                        style: TextStyle(
                            fontFamily: "OpenSans",
                            fontWeight: FontWeight.w600,
                            fontSize: 24,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 7),
                  child: Container(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Aplikasi Jurnal & Presensi",
                        style: TextStyle(
                            fontFamily: "OpenSans",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Color(0xFF8B8B8B)),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 65),
                  child: Container(
                    child: SvgPicture.asset("assets/images/logo2.svg"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 24),
                    child: TextField(
                      controller: emailController,
                      cursorColor: Colors.white,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(16),
                        labelText: 'Email',
                        hintText: 'Email',
                        labelStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14.0,
                            fontWeight: FontWeight.w400,
                            fontFamily: "OpenSans"),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 14.0,
                        ),
                        prefixIcon: Padding(
                          padding: const EdgeInsets.all(16),
                          child: SvgPicture.asset(
                            'assets/images/email.svg',
                            width: 15,
                            height: 15,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF8B8B8B), width: 2),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        floatingLabelStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.white, width: 1.5),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 24),
                    child: TextField(
                      controller: passwordController,
                      cursorColor: Colors.white,
                      style: TextStyle(color: Colors.white),
                      obscureText: obscureText,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(16),
                        labelText: 'Password',
                        hintText: 'Password',
                        labelStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontWeight: FontWeight.w400,
                          fontFamily: "OpenSans",
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 14.0,
                        ),
                        prefixIcon: Padding(
                          padding: const EdgeInsets.all(16),
                          child: SvgPicture.asset(
                            'assets/images/lock.svg',
                            width: 15,
                            height: 15,
                          ),
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              obscureText = !obscureText;
                            });
                          },
                          icon: Icon(
                            obscureText
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            color: Colors.white,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF8B8B8B), width: 2),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        floatingLabelStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.white, width: 1.5),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ),
                  ),
                ),
                // Padding(
                //   padding: EdgeInsets.only(top: 50),
                //   child: Container(
                //     margin: EdgeInsets.symmetric(horizontal: 24),
                //     padding: EdgeInsets.symmetric(horizontal: 16),
                //     decoration: BoxDecoration(
                //         border: Border.all(color: Color(0xFF8B8B8B)),
                //         borderRadius: BorderRadius.circular(8)),
                //     child: Row(
                //       children: [
                //         SvgPicture.asset(
                //           'assets/images/email.svg',
                //           width: 15,
                //           height: 15,
                //         ),
                //         SizedBox(width: 12),
                //         Expanded(
                //           child: TextFormGlobal(
                //             controller: emailController,
                //             text: "Email",
                //             textInputType: TextInputType.emailAddress,
                //             obscure: false,
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                // Padding(
                //   padding: EdgeInsets.only(top: 20),
                //   child: Container(
                //     margin: EdgeInsets.symmetric(horizontal: 24),
                //     padding: EdgeInsets.symmetric(horizontal: 16),
                //     decoration: BoxDecoration(
                //         border: Border.all(color: Color(0xFF8B8B8B)),
                //         borderRadius: BorderRadius.circular(8)),
                //     child: Row(
                //       children: [
                //         SvgPicture.asset(
                //           'assets/images/email.svg',
                //           width: 15,
                //           height: 15,
                //         ),
                //         SizedBox(width: 12),
                //         Expanded(
                //           child: TextFormGlobal(
                //             controller: passwordController,
                //             text: "Password",
                //             textInputType: TextInputType.emailAddress,
                //             obscure: true,
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),

                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 24),
                    child: ElevatedButton(
                      onPressed: () {
                        _loginUser(context, emailController.text,
                            passwordController.text);
                        // Handle login button press
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFF015FFB), // Background color
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                        minimumSize: Size(double.infinity,
                            50), // Set the button width to full width
                      ),
                      child: Text(
                        'LOGIN',
                        style: TextStyle(
                            color: Colors.white, // Text color
                            fontSize: 16,
                            fontFamily: "OpenSans",
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 95),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Lupa Password?',
                        style: TextStyle(
                            color: Colors.white, // Set the text color
                            fontSize: 14,
                            fontFamily: "OpenSans",
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                          width: 5), // Add some spacing between the two texts
                      Text(
                        'Hubungi Admin',
                        style: TextStyle(
                            color: Color(0xFF015FFB), // Set the text color
                            fontSize: 14,
                            fontFamily: "OpenSans",
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
