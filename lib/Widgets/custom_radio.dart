import 'package:flutter/material.dart';

class CustomRadio extends StatefulWidget {
  final int value;
  final int? groupValue;
  final Color activeColor;
  final Color inactiveColor;
  final Color borderColor;
  final void Function(int?)? onChanged;
  final String label;

  const CustomRadio({
    required this.value,
    required this.groupValue,
    required this.onChanged,
    required this.activeColor,
    required this.inactiveColor,
    required this.borderColor,
    required this.label,
  });

  @override
  State<CustomRadio> createState() => CustomRadioState();
}

class CustomRadioState extends State<CustomRadio> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (widget.onChanged != null) {
          widget.onChanged!(widget.value);
        }
      },
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: widget.borderColor,
            width: 1.5,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(6),
          child: Container(
            width: 10,
            height: 10,
            decoration: BoxDecoration(
              color: widget.value == widget.groupValue
                  ? widget.activeColor
                  : widget.inactiveColor,
              shape: BoxShape.circle,
            ),
          ),
        ),
      ),
    );
  }
}
