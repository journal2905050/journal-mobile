import 'package:creetech/Pengajar/Sidebar/sidebar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class DashboardPengajar extends StatefulWidget {
  const DashboardPengajar({super.key});

  @override
  State<DashboardPengajar> createState() => _DashboardPengajarState();
}

class _DashboardPengajarState extends State<DashboardPengajar> {
  late List<GDPData> _chartData;

  @override
  void initState() {
    super.initState();
    _chartData = getChartData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF191925),
        appBar: AppBar(
          backgroundColor: Color(0xFF232A3F),
          title: Text(
            'Dashboard',
            style: TextStyle(
                fontFamily: "OpenSans",
                fontSize: 20,
                fontWeight: FontWeight.w400),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: CircleAvatar(
                backgroundImage: AssetImage('assets/images/33.jpg'),
              ),
              onPressed: () {},
            ),
          ],
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: SvgPicture.asset(
                'assets/images/burger.svg',
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          }),
        ),
        drawer: SidebarPengajar(),
        body: Row(
          children: [
            Expanded(
              child: Container(
                height: 425,
                margin: EdgeInsets.all(16),
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Color(0xFF232A3F),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        'Senin',
                        style: TextStyle(
                          fontFamily: "OpenSans",
                          fontSize: 32,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Container(
                      child: Text(
                        '12 / 12 / 2023',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "OpenSans",
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    SfCircularChart(
                      legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.wrap,
                        textStyle: TextStyle(color: Colors.white),
                      ),
                      series: <CircularSeries>[
                        DoughnutSeries<GDPData, String>(
                          dataSource: _chartData,
                          xValueMapper: (GDPData data, _) => data.continent,
                          yValueMapper: (GDPData data, _) => data.gdp,
                          innerRadius: '92%',
                          pointColorMapper: (GDPData data, _) =>
                              getColorBasedOnCategory(data.continent),
                        )
                      ],
                      annotations: <CircularChartAnnotation>[
                        CircularChartAnnotation(
                          widget: Container(
                            child: Text(
                              '1200\nTotal Data',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          angle: 0,
                          radius: '0%',
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}

List<GDPData> getChartData() {
  final List<GDPData> chartData = [
    GDPData('Alpha', 50),
    GDPData('Masuk', 12),
    GDPData('Ijin', 20),
    GDPData('Sakit', 8),
  ];
  return chartData;
}

class GDPData {
  GDPData(this.continent, this.gdp);
  final String continent;
  final int gdp;
}

Color getColorBasedOnCategory(String category) {
  switch (category) {
    case 'Alpha':
      return Color(0xFFFF3636);
    case 'Masuk':
      return Color(0xFF015FFB);
    case 'Ijin':
      return Color(0xFF6A6A6A);
    case 'Sakit':
      return Color(0xFF80BB12);
    default:
      return Colors.grey; // default color or any other color
  }
}
